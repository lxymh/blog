---
title: Git配置多个SSH-Key
date: 2019-07-25 
tags:
---

**Git配置多个SSH-Key【gitee】 【gitlab】**

<!--more-->

## 任务背景

有多个git账号，要保证自己的每一篇都有更新。

## 任务要求


## 任务分解

### gitee的SSH-Key

打开终端bash

输入命令：

`ssh-keygen -t rsa -C 'xxxxx@company.com' -f ~/.ssh/gitee_id_rsa`

按回车就好了
然后切换到.shh目录下

` cd ~/.ssh/`

查看.pub的文件

![](Git配置多个SSH-Key/gitee01.png)

复制以下内容

![](Git配置多个SSH-Key/gitee02.png)

粘贴到Gitee帐号上的SSH-Key中

![](Git配置多个SSH-Key/gitee03.png)



### gitlab的SSH-Key

`ssh-keygen -t rsa -C 'xxxxx@outlook.com' -f ~/.ssh/gitlab_id_rsa`

然后切换到.shh目录下

` cd ~/.ssh/`

查看.pub的文件
将SSH-Key粘贴到Gitee帐号上的SSH-Key中

![](Git配置多个SSH-Key/gitlab01.png)



在.ssh文件夹下新建一个config文件

然后输入以下内容

```
# gitee
Host gitee.com
HostName gitee.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/gitee_id_rsa
# gitlab
Host gitlab.com
HostName gitlab.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/gitlab_id_rsa
```

### 测试

`ssh -T git@gitee.com`

`ssh -T git@gitlab.com`

![](Git配置多个SSH-Key/gitee04.png)

成功！！！

## push前的准备

在00-javasebasiccode目录下新建.gitignore文件，将不想上传的文件写上

```
.gitignore

*.DS_Store
Thumbs.db

.idea/
out/
*.iml
```

## 一次push到两个仓库

`git config use.name"xxxxxx"`

`git config use.email"xxxxxxxxxxx@outlook.com"`

`git remote add gitlab git@gitlab.com:javadeveloperlearnroadmap/00-javasebasiccode.git`

`git push - u gitlab master`

`git remote add gitee git@gitee.com:JavaLearnRoadmap/00-JavaSEBasicCode.git`

`git push - u gitee master`
