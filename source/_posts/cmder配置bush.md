---
title: cmder配置bush
date: 2019-07-25
tags:
---

**cmder配置bush**

<!--more-->

## 任务背景

git的上传与配置使用bush更好用、更方便一些
cmder比较简单好用

## 任务要求

- 学会自己配置cmder
- 设置自己需要的默认终端
- 并且增加到右键菜单

## 任务分解

### 下载cmder

{% note default %}
**cmder**[下载链接](https://cmder.net/)
{% endnote %}



![](cmder配置bush/Dowload001.png)

我下载的是Mini版本，一个zip文件，解压后使用

![](cmder配置bush/解压完成001.png)

完成后，配置系统变量

### 系统变量配置

找到计算机的系统属性点击高级，其中有一个环境变量

![](cmder配置bush/配置系统变量01.png)

在系统变量中配置Path（双击即可）

![](cmder配置bush/配置系统变量02.png)

回到安装目录

![](cmder配置bush/配置系统变量03.png)

复制路径

![](cmder配置bush/配置系统变量04.png)

回到Path环境变量，新建变量，输入刚刚的复制的路径

![](cmder配置bush/配置系统变量05.png)

添加完成后

![](cmder配置bush/配置系统变量06.png)

### 换bash路径

因为下载的是Mini版本的所以bash是不可用的，我们需要换一下路径。
我在之前已经下载过bash

![](cmder配置bush/bash路径.png)

复制bash所在的路径

![](cmder配置bush/bash路径02.png)

打开cmder打开菜单，点击Settings

![](cmder配置bush/bash路径03.png)

打开其中的bash

![](cmder配置bush/bash路径04.png)

更换掉里面的路径

![](cmder配置bush/bash路径05.png)

保存退出

![](cmder配置bush/bash路径06.png)

### 设置默认使用

还是Settings页面进行以下操作,我设置的是bash

![](cmder配置bush/bash默认.png)

保存退出！

### 将cmder添加到右键

在cmd窗口运行，需要管理员权限

![](cmder配置bush/添加到右键01.png)

输入以下命令：

`Cmder.exe /REGISTER ALL`

鼠标在桌面右击一下

![](cmder配置bush/添加到右键02.png)

ok!!!!!