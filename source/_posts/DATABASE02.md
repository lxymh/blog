---
title: DATABASE02
date: 2019-08-04 
tags:
---

**DATABASE**

<!--more-->

### 任务背景



### 任务要求



### 任务分解

* 聚合 + 分组
* 字符串/日期/时间操作
* 输出控制 + 重定向
* 嵌套查询
* 公共表表达式
* 窗口函数

### 知识储备

#### **SQL历史**

* 在1970年代初，由IBM公司San Jose,California研究实验室的埃德加·科德发表将数据库组成表格的应用原则（Codd's Relational Algebra)。1974年，同一实验室的D.D.Chamberlin和R.F. Boyce对Codd's Relational Algebra在研制关系数据库管理系统System R中，研制出一套规范语言-SEQUEL，并在1976年11月的IBM Journal of R&D上公布新版本的SQL。1980年改名为SQL。
* 1979年ORACLE公司首先提供商用的SQL，IBM公司在DB2和SQL/DS数据库系统中也实现了SQL。
* 1986年10月，美国ANSI采用SQL作为关系数据库管理系统的标准语言，后为国际标准化组织(ISO)，采纳为国际标准。
* 1989年，美国ANSI采纳在ANSI X3.135-1989报告中定义的关系数据库管理系统的SQL标准语言，称为ANSI SQL 89， 该标准替代ANSI X3.135-1986版本。该标准为下列组织所采纳：
    * 国际标准化组织（ISO）
    * 美国联邦政府
* 目前，所有主要的关系数据库管理系统支持某些形式的SQL， 大部分数据库打算遵守ANSI SQL89标准。

->SQL:2016->JSON,Polymorphic tables
->SQL:2011->Temporal DBs, Pipelined DML 
->SQL:2008->TRUNCATE, Fancy ORDER
->SQL:2003->XML, windows, sequences, auto-generated IDs.
->SQL:1999->Regex, triggers, OO

#### **RELATIONAL LANGUAGES**

* Data Manipulation Language (DML)  数据操纵语言
* Data Definition Language (DDL)    数据定义语言
- Data Control Language (DCL)       数据控制语言

{% note info %}
重要提示:
    SQL基于包(重复)，而不是集合(没有重复)。
{% endnote %}

#### 聚合

AVG：平均值
MIN:最小值
MAX:最大值
SUM:求和
COUNT:统计个数








### 任务实施



### 今日目标打卡