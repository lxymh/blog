---
title: DATABASE-01
date: 2019-08-04
tags: 
---

**DATABASE**

<!--more-->

### 任务背景

在不会英语的情况下，学习卡内基梅隆大学的数据库导论（2018），C++很low，数据库还没学。

### 任务要求

* 做其中的一个digital music store
(Create a database that models a digital music store to keep track of artists and albums)
<!-- 创建一个数字音乐商店的数据库，以跟踪艺术家和专辑 -->

#### DATA INTEGRITY

* How do we ensure that the artist is the same for each album entry?
<!-- 我们如何确保每个专辑条目的艺术家相同？ -->

* What if somebody overwrites the album year with an invalid string?
<!-- 如果有人用无效的字符串覆盖了专辑年份怎么办？ -->

* How do we store that there are multiple artists on an album?
<!-- 我们如何存储专辑中有多位艺术家？ -->

#### IMPLEMENTATION

* How do you find a particular record?
<!-- 如何找到一个特定的记录? -->

* What if we now want to create a new application that uses the same database?
<!-- 如果我们现在想创建一个使用相同数据库的新应用程序呢? -->

* What if two threads try to write to the same file at the same time?
<!-- 如果两个线程试图同时写入同一个文件，该怎么办? -->

### 任务分解

做一个 digital music store。

了解DBMS

回答上述问题。

### 知识储备

|   英语    | 汉语              |
|-----------|------------------|
|database   |数据库            |
|organize   |组织；使有系统化   |
|collection |采集，聚集         |
|DML        |数据操纵语言       |
|DQL        |数据查询语言       |
|DDL        |数据定义语言       |
|DCL        |数据控制语言       |
|AVG        |平均值            |
|definition |定义              |
|creation   |创造；创作         |
|querying   |查询              | 
|update     |更新；校正         |
|administration|管理           |
|machine learning|机器学习      |
|relational model|关系模型      |
|algebra    |代数学             |
|intersection|交集              |
|product    |结果               |
|rename     |重命名             |
|assignment |分配；任务；作业    |
|aggregation|聚合；聚集         |
|sorting    |排序；整理         |

数据库是大多数计算机应用程序的核心组成部分。

**DBMS**

数据库管理系统(Database Management System)是一种操纵和管理数据库的大型软件，用于建立、使用和维护数据库，简称DBMS。它对数据库进行统一的管理和控制，以保证数据库的安全性和完整性。

数据库管理系统是一个能够提供数据录入、修改、查询的数据操作软件，具有数据定义、数据操作、数据存储与管理、数据维护、通信等功能，且能够允许多用户使用。另外，数据库管理系统的发展与计算机技术发展密切相关。而且近年来，计算机网络逐渐成为人们生活的重要部分。





### 任务实施

**Create a database that models a digital music store to keep track of artists and albums**

|艺术家|出生年份|国籍|专辑名称|专辑中的作品|发行年份|
|------|-------|----|-------|-----------|-------|
|name1 |1987   |USA |album1 |...        |1999   |
|name1 |1987   |USA |albumA |...        |2000   |
|name1 |1989   |UK  |album2 |...        |1998   |
|name1 |1982   |USA |album3 |...        |1995   |

**How do we ensure that the artist is the same for each album entry?**

**What if somebody overwrites the album year with an invalid string?**

**How do we store that there are multiple artists on an album?**

可以在该数据元组中添加。

**How do you find a particular record?**

使用搜索引擎搜索。


### 今日目标打卡

做一个 digital music store。

了解DBMS

#### 关于问题

暂时搞不懂，正在学习。