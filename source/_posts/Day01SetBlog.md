---
title: Day01搭建博客
date: 2019-03-24 18:06:05
tags:
---
# 一、安装前准备
## 安装node.js
	以Windows环境安装node.js为例，首先登录node.js官网，选择适合自己的版本进行下载，然后进行安装。
	
## 镜像的使用
	使用淘宝的https://npm.taobao.org/
	
	使用命令
	$ npm install -g cnpm --registry=https://registry.npm.taobao.org



# 二、开始搭建博客
	新建blog文件夹。
	建一个公开的github项目，然后将blog导入。
	git clone https://github.com/theme-next/hexo-theme-next themes/next
	构建
	hexo g
	跑一下
	hexo s
	打开
	http://localhost:4000
  
	配置主题：
	https://hexo.io/themes/
	选择一个nexT的主题。
  
  
    先构建、预览一下
    hexo g |hexo s
    预览一下
  
  停掉；ctrl +c
  
  加git地址
  
  进入https://app.netlify.com/sites/angry-brattain-5329a9/deploys/5c9633ebc51ba64bdfa0bedc
  新建站点gitlab
  blog
  直接部署，直接点下一步
  
  deploy log 
  部署日志
  
  预览
  