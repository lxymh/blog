---
title: '小学期学习mfc'
date: 2019-06-23 20:37:53
tags: 小学期
---

## 为什么要写学习MFC呢

很多公司往往都有自己的类库、框架，如果你不了解类库，你就无从下手，这是我们学习类库、框架设计得的原因。
MFC：微软基础类库
如果想要在Windows平台上做GUI开发，MFC事一个很好的选择。
学习MFC不能只学习用MFC，还要学习MFC的框架设计思想。

## 基本概念解释

1.sdk和API：
    **SDK**：软件开发工具包（Software Development Kit）,一般都是一些被软件工程师用于为特定的软件包、软件框架、硬件平台、操作系统等建立应用软件的开发工具整合。
    **API 函数**：Windows操作系统提供给应用程序编程的接口
    （Application Programming Interface）
    *Windows应用程序API函数是通过C语言实现的，所有主要的 Windows 函数都是在Widows.h 头文件中进行了声明*

2.窗口和句柄
    窗口是Windows 应用程序中一个非常重要的元素，一个Widows 应用程序至少要有一个窗口，成为主窗口。
    窗口是屏幕上的一块矩形区域，是 Wisows 应用程序与用户进行交互的接口。利用窗口可以接收用户的输入、以及显示输出。
    窗口可以分为客户区和非客户区，窗口可以有一个父窗口，有父窗口的窗口称为子窗口。对话框和消息框也是一种窗口。在对话框上通常还包含许多子窗口。
    *在Windows 应用程序中，窗口是通过窗口句柄（HWND）来标识的。我们要对某个窗口进行操作，首先就要得到这个窗口的句柄。*
    句柄（HANDLE）是widows程序中一个重要的概念，使用频繁。在Windows程序中，有各种各样的资源（窗口、图标、光标、画刷等），系统在创建这些资源时会为他们分配内存，并返回标识这些资源的标识号，即句柄。

3.消息与消息队列

Windows程序设计是一种完全不同于传统的DOS方式的程序设计方法。它是一种事件驱动方式的程序设计模式，主要是基于信息的。
每一个Windows应用程序开始执行后，系统都会为该程序创建一个消息队列，这个消息队列用来存放该程序创建的窗口的消息。

 4.WinMain函数

当Windows操作系统启动一个程序时，他调用的就是该程序的Win Main函数。
WinMain是Windows程序的入口点函数，与DOS程序的入口点函数main的作用相同，当WinMain函数结束或返回时，Windows应用程序结束。

## Windows 编程模型

一个完整的Win32程序（`#include<windows.h>`)，该程序的功能是创建一个窗口，并在该窗口响应键盘及鼠标信息，程序的实现步骤为：
   1）WinMain函数的定义
   2）创建一个窗口
   3）进行消息循环
   4）编写窗口过程函数

## MFC 入门

### MFC是什么

微软基础类库（Microsoft Foundation Classes，简称MFC）是一个微软公司提供的类库（class libraries），以C++类的形式封装了Windows API，并且包含一个应用程序框架，以减少应用程序开发人员的工作量。其中包含的类包含大量Windows 句柄封装类和很多Windows的内建控件和组件的封装类。
MFC把Windows SDK API 函数包装成了几百个类，MFC给Windows 操作系统提供了面向对象的接口，支持可重用性、自包含性以及其他的OPP原则。MFC通过编写类来封装窗口、对话框以及其他对象，引入某些关键的虚函数来完成，并且MFC设计者使类库带来的总开销降到了最低。

### 编写第一个MFC应用程序
