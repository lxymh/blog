---
title: ubuntu镜像下载及虚拟机创建
date: 2019-07-27 
tags: 
---

**ubuntu镜像下载及VM虚拟机创建**

<!-- more -->

## 任务背景

在准备做项目的情况下，先配置好虚拟机

## 任务实施

### ubuntu镜像下载

[下载地址](http://mirrors.163.com/ubuntu-releases/)，我安装的是18.04.2版本的

下载完成后，打开VMware Workstation Pro开始创建虚拟机

### 创建新的Ubuntu虚拟机

打开VMware Workstation Pro，创建新的虚拟机，选择自定义配置

![](ubuntu镜像下载及虚拟机创建/新建虚拟机001.png)

选择稍后安装操作系统

![](ubuntu镜像下载及虚拟机创建/新建虚拟机002.png)

客户机操作系统选择Linux，版本选择 64位

![](ubuntu镜像下载及虚拟机创建/新建虚拟机003.png)

给自己的虚拟机起名字，并且把它放到合适的位置。

![](ubuntu镜像下载及虚拟机创建/新建虚拟机004.png)

选择一个处理器，两个内核就ok了

![](ubuntu镜像下载及虚拟机创建/新建虚拟机005.png)

内存选2G就ok

![](ubuntu镜像下载及虚拟机创建/新建虚拟机006.png)

网络类型选择使用NAT

![](ubuntu镜像下载及虚拟机创建/新建虚拟机007.png)

I/O控制器选择推荐就OK

![](ubuntu镜像下载及虚拟机创建/新建虚拟机008.png)

磁盘类型也是推荐的SCSI

![](ubuntu镜像下载及虚拟机创建/新建虚拟机009.png)

然后给虚拟机创建新虚拟磁盘

![](ubuntu镜像下载及虚拟机创建/新建虚拟机010.png)

磁盘大小是20G，然后将虚拟磁盘拆分成多个文件

![](ubuntu镜像下载及虚拟机创建/新建虚拟机011.png)

磁盘文件就不改了，然后完成。

导入刚刚下好的镜像

![](ubuntu镜像下载及虚拟机创建/新建虚拟机012.png)

![](ubuntu镜像下载及虚拟机创建/新建虚拟机013.png)

完成后，回到主页，配置网络

### 网络配置

编辑-->虚拟网络编辑器

![](ubuntu镜像下载及虚拟机创建/网络配置001.png)

点击“更改设置”，这个需要管理员权限，在弹出的页面点击确定就好了

![](ubuntu镜像下载及虚拟机创建/网络配置002.png)

然后添加网络，添加一个VMnet2就行，将VMnet2改成NAT模式，VMnet8改成仅主机模式，子网IP改成192.168.80.0

![](ubuntu镜像下载及虚拟机创建/网络配置003.png)

![](ubuntu镜像下载及虚拟机创建/网络配置004.png)

然后NAT设置，将网关的IP改掉，第三个改成80

![](ubuntu镜像下载及虚拟机创建/网络配置005.png)

编辑虚拟机，将网络适配器改成刚刚添加的VMnet2（NAT模式）

![](ubuntu镜像下载及虚拟机创建/网络配置006.png)

修改本地网络的DNS服务器地址

![](ubuntu镜像下载及虚拟机创建/网络配置007.png)

### 开启虚拟机

这个版本没有中文，因为是做服务器的，所以选择英语就ok了，一直继续到第六页，在这里我们用阿里云的镜像,按Tab键换到输入框更改

`http://mirrors.aliyun.com/ubuntu/`

![](ubuntu镜像下载及虚拟机创建/虚拟机设置001.png)

选择LVM将磁盘该到最大

![](ubuntu镜像下载及虚拟机创建/虚拟机设置002.png)

![](ubuntu镜像下载及虚拟机创建/虚拟机设置003.png)

设置自己的名称和密码，按回车，安装完他会自己重启一遍，然后登录一下，没问题了关机`poweroff`
在编辑虚拟机中，移除CD/DVD；
将虚拟机导出为OVF

![](ubuntu镜像下载及虚拟机创建/导出为OVF.png)

自己创建个目录就ok

### 在Base的基础上克隆一个Dcoker

鼠标右键点击你的虚拟机-->管理-->克隆

![](ubuntu镜像下载及虚拟机创建/Docker001.png)

将其中的克隆方法改成链接克隆就好了，路径自己设置一个

![](ubuntu镜像下载及虚拟机创建/Docker002.png)

重新生成一下Docker 的 MAC 地址

![](ubuntu镜像下载及虚拟机创建/Docker003.png)

进入Base 安装openSSH
`apt install openssh-server`

开启虚拟机root用户权限

设置管理员密码：

`sudo passwd`

切换到管理员用户： 

`su`

修改root的权限配置文件

`vim/ etc/ssh/sshd_config`

修改为：[其中按“i”为插入，按键Esc为退出，按`:`输入`wq!`为保存并退出， *注意：使用英文输入法*]

![](ubuntu镜像下载及虚拟机创建/Docker004.png)

重启SSH服务器

`service ssh restart`

看一下IP

`ip a`

运行远程工具Xshell


```
network:
    ethernets:
        ens32:  
         addresses: [192.168.80.132/24]
         gateway4: 192.168.80.2
         nameservers:
           addresses:
           - 114.114.114.144
           - 114.114.115.115
    version: 2
```

