---
title: 00-JavaSE基础-Day05【数组】
date: 2019-06-11 
tags: 00-JavaSE基础
---

**数组**

<!-- more -->

## 今日内容

- 数组概念
- 数组的定义
- 数组的索引
- 数组内存
- 数组的遍历
- 数组的最大值获取
- 数组反转
- 数组作为方法参数和返回值

## 数组定向和访问

### 容器概念

- 容器：是将多个数据存储到一起，每个数据称为该容器的元素。
- 生活中的容器： 水杯，衣柜，教室 

### 数组概念

数组就是存储数据长度固定的容器，保证多个数据和数据类型要一致。

### 数组的定义

###方式一

 - 格式：
  `数组存储的数据类型[] 数组名字 = new 数组存储数据类型[长度];`
 - 注意：数组有定长特性，长度一旦指定，不可更改。
 - 举例：

  定义可以存储3个整数的数组容器，代码如下：

  ` int[] arr = new int[3];`

### 方式二

 - 格式：
  ` 数据类型[] 数组名 = new 数据类型[]{元素1，元素2，元素3···};`

  - 举例：
  定义存储1，2，3，4，5整数的数组容器

  ` int[] arr = {1,2,3,4,5};`

### 数组的访问

 - 索引：每一个存储到数组的元素，都会自动拥有一个编号，从0开始，这个自动编号称为**数组索引(index)**,可以通过数组的索引访问到数组中的元素。
 - 格式：
  `数组名[索引]`

 - 数组的长度属性：每个数组都具有长度，而且是固定的，Java中赋予了数组的一个属性，可以获取到数组的长度，语句为:`数组名.length`,属性length的执行结果是数组的长度，int类型结果。由此推断，数组的最大索引值为`数组名.length-1`。

```java
public static void main(String[] args) {
    int[] arr = new int[]{1,2,3,4,5};
    //打印数组属性，输出的结果是5
    System.out.println(arr.length);
}
```

 - 索引访问数组中的元素：
    * 数组名[索引]=数值，为数组中的元素赋值
    * 变量 = 数组名[索引]，或去除数组中的元素

```java
public static void main(System[] args) {
    //定义存储int类型数组,赋值元素1，2，3，4，5
    int[] arr = {1,2,3,4,5};
    //为0索引=元素赋值为6
    arr[0] = 6;
    //获取数组0为索引上的元素
    int i = arr[0];
    System.out.println(i);
    //直接输出数组0索引的元素
    System.out.println(arr[0]); 
}
```

## 数组原理内存图

### 内存概述

内存是计算机中的重要原件，临时存储区域，作用是运行程序。我们编写的程序是存放在硬盘中的，在硬盘中的程序是不会运行的，必须放进内存中才能运行，运行完毕后会清空内存
Java虚拟机要运行程序，必须要对内存进行空间的分配和管理。
### Java虚拟机的内存划分

为了提高运算效率，就对空间进行了不同区域的划分，因为每一片区域都有特定的处理数据方法和内存管理方式。

- JVM的内存划分

![](2019-6-11/01-Java中的内存划分.png)

### 2.3 数组在内存中的存储

### 一个数组内存图

```java
public static void main(String[] args) {
    int[] arr = new int[3];
    System.out.println(arr);//[I@5f150435
}
```

以上方法执行，输出的结果是[I@5f150435，这个是什么呢？是数组在内存中的地址。new出来的内容都是在堆内存中存储的，而方法中的变量arr保存的是数组的地址。

**输出arr[0],就会输出arr保存的内存地址中数组0索引上的元素。**

![](2019-6-11/02-只有一个数组的内存图.png)

### 两个数组的内存图

```java
public static void main(String[] args) {
    int[] arr = new int[3];
    int[] arr2 = new int[2];
    System.out.println(arr);
    System.out.pringln(arr2);
}
```

![](2019-6-11/03-有两个独立数组的内存图.png)

### 两个变量指向一个数组

```java
public static void main(String[] args) {
    //定义数组，存储3个元素
    int[] arr = new int[3];
    arr[0] = 5;
    arr[1] = 6;
    arr[2] = 7;
    System.out.println(arr[0]);
    System.out.println(arr[1]);
    System.out.println(arr[2]);
    int[] arr2 = arr;
    arr2[1] = 9;
    System.out.println(arr[1]);
}
```

![](2019-6-11/04-两个引用指向同一个数组的内存图.png)

## 数组的常见操作

### 数组越界异常

例题：观察一下代码，运行后会出现什么结果

```java
public static void main(String[] args) {
    int[] arr = {1,2,3};
    System.out.println(arr[3]);
}
```

解析：由于数组中只赋值了三个值，所以数组长度是3，索引为0.1.2，没有3的索引，因此我们不能访问数组中不存在的索引，程序运行后，会抛出*ArrayIndexOutOfBoundsException*数组越界异常。在开发中，数组越界异常时不能出现的。

### 数组空指针异常

观察一下代码，运行后会出现什么结果。

```java
public static void main(String[] args) {
    int[] arr = {1,2,3};
    arr = null;
    System.out.println(arr[0]);
}
```

![](2019-6-11/09-数组的空指针异常错误.png)

`arr = null`这行代码，意味着变量arr将不会在保存数组的内存地址，也就不允许在操作数组了，因此运行的时候会抛出` NullPointerException `空指针异常。在开发中，数组的空指针异常是不能出现的。
**空指针异常在内存图中的表现**

![](2019-6-11/08-数组的空指针异常.png)

### 数组遍历 【重点！！！】

- 数组遍历：是将数组中的每个元素分别获取出来，就是遍历。遍历也是数组操作中的基石。

```java
public class demo0202 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
        System.out.println(arr[3]);
        System.out.println(arr[4]);
    }
}
```

也可以用循环遍历。

### 数组获得最大元素

- 最大值获得：从数组中所有元素中找出最大值。
- 实现思路：
    * 定义变量，保存数组0索引上的元素
    * 遍历数组，获取出数组中的每个元素
    * 将遍历到的元素和保存数组0索引上值的变量进行比较
    * 如果数组元素大于变量的值，就替换它
    * 数组循环遍历结束，变量保存的就是数组中的最大值

![](2019-6-11/06-比武招亲的示意图.png)

### 数组反转

- 数组的反转：数组中的元素颠倒顺序，例如原始数组为1，2，3，4，5，反转后为5，4，3，2，1
- 实现思想：
   * 实现反转，就需要将数组最远端的元素互换
   * 定义两个变量，保存数组的最小索引和最大索引
   * 两个索引上的元素呼唤
   * 最小索引++，最大所引--，在次交换位置
   * 最小索引超过了最大索引，结束

![](2019-6-11/07-数组元素反转的思路.png)

## 数组作为方法参数和返回值

### 数组作为方法参数

以前的方法中我们学习了方法的参数和返回值，但是都是基本数据类型，那么作为引用类型的数组能否作为方法的参数进行传递呢，当然是可以的
- 数组作为方法传递参数，传递的参数是数组内存的地址。

```java
public class demo01 {
        public static void main(String[] args) {
            int[] arr = {1, 3, 5, 7 ,9 };
            //调用方法，传递数组
            printArray(arr);
        }

        public static void printArray(int[] arr) {
            for (int i = 0;i < arr.length; i++) {
                System.out.println(arr[i]);
            }
        }
    }

```

- ![](2019-6-11/10-数组作为方法传递参数.png)

### 数组作为方法返回值

- 数组作为方法的返回值，返回的是数组的内存地址

```java
public class demo05 {
    public static void main(String[] args) {
        //调用方法，接受数组的返回值
        //接收到的是数组的内存地址
        int[] arr = getArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
    /*
    创建方法，返回值是数组类型
    return返回数组的地址
     */
    public static int[] getArray() {
        int[] arr = {1, 3, 5, 7, 9 };
        //返回数组地址，返回到调用值
        return arr;
    }
}
```

![](2019-6-11/11-数组作为方法的返回值.png)

### 方法的参数类型区别

**代码分析**

1.分析下列程序代码，计算输出结果。

```java
public class demo06 {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        System.out.println(a);
        System.out.println(b);
        change(a, b);
        System.out.println(a);
        System.out.println(b);
    }
    public static void change(int a, int b) {
        a = a + b;
        b = b + a;
    }
}
```

输出结果1212

2.分析下列程序代码，计算输出结果

```java
package day01;

public class demo07 {
    public static void main(String[] args) {
        int[] arr = {1, 3, 5};
        System.out.println(arr[0]);
        change(arr);
        System.out.println(arr[0]);
    }

    public static void change(int[] arr) {
        arr[0] = 200;
    }
}
```

{% note info %}
总结：
**方法的参数为基本类型时，传递的是数据值。方法的参数为引用类型时，传递的是地址值**
{% endnote %}

